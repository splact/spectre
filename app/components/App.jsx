import React, { Component } from 'react';


export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.uploader = null;

    this.audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    this.gainNode = this.audioCtx.createGain();
    this.gainNode.connect(this.audioCtx.destination);
    this.audioSource = null;

    // binding methods
    this.onAudioChange = this.onAudioChange.bind(this);
  }

  getAudioData(audioUrl) {
    this.setState({
      url: audioUrl,
      duration: null,
      length: null,
      numberOfChannels: null,
      sampleRate: null,
      isLoading: true,
    });

    this.audioSource = this.audioCtx.createBufferSource();
    const request = new XMLHttpRequest();

    request.open('GET', audioUrl, true);
    request.responseType = 'arraybuffer';
    request.onload = () => {
      const audioData = request.response; // arrayBuffer

      this.audioCtx.decodeAudioData(audioData)
        .then(buffer => {
          this.audioSource.buffer = buffer;

          this.audioSource.connect(this.gainNode);
          this.audioSource.loop = true;

          this.setState({
            duration: buffer.duration,
            length: buffer.length,
            numberOfChannels: buffer.numberOfChannels,
            sampleRate: buffer.sampleRate,
            isLoading: false,
          });
        })
        .catch(e => {
          console.log('Error with decoding audio data', e.err);
        });
    };

    request.send();
  }

  onAudioChange() {
    const files = this.uploader.files;

    if (files.length) {
      const audioUrl = (URL || webkitURL).createObjectURL(files[0]);

      this.getAudioData(audioUrl);
    }
  }

  render() {
    const { duration, length, numberOfChannels, sampleRate, isLoading } = this.state;

    return <main>
      <input ref={r => this.uploader = r} type="file" accept="audio/*" onChange={this.onAudioChange}/>
      {!isLoading && (
        <div>
          <p>Duration: {duration}</p>
          <p>Length: {length}</p>
          <p>Number of channels: {numberOfChannels}</p>
          <p>Sample rate: {sampleRate}</p>
        </div>
      )}
    </main>;
  }
}
