const BundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = (options) => ({
  webpack(config) {
    if (options.analyze) {
      config.plugins.push(
        new BundleAnalyzer()
      );
    }

    return config;
  },
  autoprefixer: {
    browsers: ['last 3 versions']
  },
  devServer: {
    https: true,
  }
});
